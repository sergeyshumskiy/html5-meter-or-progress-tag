<?php

namespace Drupal\html5_meter_progress\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'HTML5 Meter' formatter.
 * @FieldFormatter(
 *   id = "field_html5_meter_formatter",
 *   label = @Translation("HTML5 Meter"),
 *   field_types = {
 *     "field_html5_meter_progress"
 *   }
 * )
 */
class HTML5MeterFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'min' => '0',
        'max' => '100',
        'low' => '25',
        'high' => '65',
        'optimum' => '90',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum'),
      '#default_value' => $this->getSetting('min'),
      '#description' => $this->t('The minimum value on the scale.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];
    $element['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum'),
      '#default_value' => $this->getSetting('max'),
      '#description' => $this->t('The maximum value on the scale.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];
    $element['low'] = [
      '#type' => 'number',
      '#title' => $this->t('Low'),
      '#default_value' => $this->getSetting('low'),
      '#description' => $this->t('The upper-boundary of the low section on the scale.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];
    $element['high'] = [
      '#type' => 'number',
      '#title' => $this->t('High'),
      '#default_value' => $this->getSetting('high'),
      '#description' => $this->t('The lower-boundary of the high section on the scale.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];
    $element['optimum'] = [
      '#type' => 'number',
      '#title' => $this->t('Optimum'),
      '#default_value' => $this->getSetting('optimum'),
      '#description' => $this->t('The optimum value.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $min = $this->getSetting('min');
    if ($min === '0' || !empty($min)) {
      $summary[] = $this->t('min: @min', ['@min' => $min]);
    }
    else {
      $summary[] = $this->t('No min');
    }

    $max = $this->getSetting('max');
    if ($max === '0' || !empty($max)) {
      $summary[] = $this->t('max: @max', ['@max' => $max]);
    }
    else {
      $summary[] = $this->t('No max');
    }

    $low = $this->getSetting('low');
    if ($low === '0' || !empty($low)) {
      $summary[] = $this->t('low: @low', ['@low' => $low]);
    }
    else {
      $summary[] = $this->t('No low');
    }

    $high = $this->getSetting('high');
    if ($high === '0' || !empty($high)) {
      $summary[] = $this->t('high: @high', ['@high' => $high]);
    }
    else {
      $summary[] = $this->t('No high');
    }

    $optimum = $this->getSetting('optimum');
    if ($optimum === '0' || !empty($optimum)) {
      $summary[] = $this->t('optimum: @optimum', ['@optimum' => $optimum]);
    }
    else {
      $summary[] = $this->t('No optimum');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $settings = [
        'min' => $this->getSetting('min'),
        'max' => $this->getSetting('max'),
        'low' => $this->getSetting('low'),
        'high' => $this->getSetting('high'),
        'optimum' => $this->getSetting('optimum'),
      ];

      $elements[$delta] = [
        '#theme' => 'meter_formatter',
        '#meter' => $item->getValue(),
        '#settings' => $settings,
      ];
    }

    return $elements;
  }

  /**
   * Custom Formatter settings validation callback.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function settingsFormValidate($element, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    $field_name = $form_state->getStorage()['plugin_settings_edit'];
    $min = $input['fields'][$field_name]['settings_edit_form']['settings']['min'];
    $max = $input['fields'][$field_name]['settings_edit_form']['settings']['max'];
    $low = $input['fields'][$field_name]['settings_edit_form']['settings']['low'];
    $high = $input['fields'][$field_name]['settings_edit_form']['settings']['high'];
    $optimum = $input['fields'][$field_name]['settings_edit_form']['settings']['optimum'];

    if ($min > $max) {
      $form_state->setError($element, $this->t("Parameter Min should be less than $max"));
    }

    if ($low < $min || $high < $min || $optimum < $min || $low > $max || $high > $max || $optimum > $max) {
      $form_state->setError($element, $this->t("Parameters: Low, High, Optimum should be in the range [$min ... $max]"));
    }
  }

}
