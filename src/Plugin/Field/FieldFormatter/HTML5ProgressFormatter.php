<?php

namespace Drupal\html5_meter_progress\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'HTML5 Progress' formatter.
 *
 * @FieldFormatter(
 *   id = "field_html5_process_formatter",
 *   label = @Translation("HTML5 Progress"),
 *   field_types = {
 *     "field_html5_meter_progress"
 *   }
 * )
 */
class HTML5ProgressFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'max' => '100',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum'),
      '#default_value' => $this->getSetting('max'),
      '#description' => $this->t('The maximum value on the scale.'),
      '#element_validate' => [
        [$this, 'settingsFormValidate'],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $max = $this->getSetting('max');
    if ($max === '0' || !empty($max)) {
      $summary[] = $this->t('max: @max', ['@max' => $max]);
    }
    else {
      $summary[] = $this->t('No max');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $settings = [
        'max' => $this->getSetting('max')
      ];

      $elements[$delta] = [
        '#theme' => 'process_formatter',
        '#process' => $item->getValue(),
        '#settings' => $settings
      ];
    }

    return $elements;
  }

  /**
   * Custom Formatter settings validation callback.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function settingsFormValidate($element, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    $field_name = $form_state->getStorage()['plugin_settings_edit'];
    $max = (int) $input['fields'][$field_name]['settings_edit_form']['settings']['max'];

    if ($max <= 1) {
      $form_state->setError($element, $this->t("Parameter Max should be more than 1"));
    }
  }

}
