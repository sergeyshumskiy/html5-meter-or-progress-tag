<?php

namespace Drupal\html5_meter_progress\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'HTML5 meter/progress' widget.
 *
 * @FieldWidget(
 *   id = "field_html5_meter_progress_widget",
 *   label = @Translation("HTML5 meter/progress"),
 *   field_types = {
 *     "field_html5_meter_progress"
 *   }
 * )
 */
class HTML5MeterProgressWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = [
      '#type' => 'number',
      '#title' => $this->t('Value of progress'),
      '#description' => $this->t('The measurement on the scale.'),
      '#required' => $element['#required'],
    ];

    if (!$items[$delta]->isEmpty()) {
      $element['value']['#default_value'] = $items[$delta]->getValue()['value'];
    }
    return $element;
  }

}
