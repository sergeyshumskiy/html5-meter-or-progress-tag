<?php
namespace Drupal\html5_meter_progress\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of field_meter_progress.
 *
 * @FieldType(
 *   id = "field_html5_meter_progress",
 *   label = @Translation("HTML5 meter/progress"),
 *   default_formatter = "field_html5_meter_formatter",
 *   default_widget = "field_html5_meter_progress_widget",
 * )
 */
class HTML5MeterProgressItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'Your value of measurement.',
          'type' => 'numeric',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setDescription(t('Your value of measurement.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '';
  }

}
